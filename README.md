# BlueArchive ST content preload pack
To be used with [the bootstrapper](https://gitlab.com/schale/SillyTavern-Bootstrapper).

## Installation
Windows:
- just download the `preload.json` file and put it next to the start script

Linux:
```bash
wget -O preload.json "https://gitlab.com/schale/bapreload/-/raw/main/preload.json"
# or
curl -o preload.json "https://gitlab.com/schale/bapreload/-/raw/main/preload.json"

# launch the bootstrapper as usual
```